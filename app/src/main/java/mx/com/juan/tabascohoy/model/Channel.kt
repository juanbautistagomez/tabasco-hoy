package mx.com.juan.tabascohoy.model

import android.media.Image
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

class Channel {
    @Root(name = "title")
    private var title: String? = null
    @Root(name = "link")
    private var link: String? = null
    @Root(name = "language")
    private var language: String? = null
    @Root(name = "copyright")
    private var copyright: String? = null
    @Root(name = "description")
    private var description: String? = null
    @Root(name = "Image")
    var ImageObject: Image? = null
    @Root(name = "item")
    var item = ArrayList<Any>()


}