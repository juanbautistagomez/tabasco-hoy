package mx.com.juan.tabascohoy.endpoint

import mx.com.juan.tabascohoy.model.Channel
import mx.com.juan.tabascohoy.model.TabascoHoychannel
import org.simpleframework.xml.Root
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiClientInterface {

    @GET("servicios/secciones/rss_1_1_0.xml")
    fun tabascoHoyBase(): Call<Channel>
}