package mx.com.juan.tabascohoy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import mx.com.juan.tabascohoy.endpoint.ApiClient
import mx.com.juan.tabascohoy.model.Channel
import mx.com.juan.tabascohoy.model.TabascoHoychannel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tabascoHoy()
    }

    fun tabascoHoy() {
        val call: Call<Channel> = ApiClient.getClient.tabascoHoyBase()
        call.enqueue(object : Callback<Channel> {
            override fun onFailure(call: Call<Channel>, t: Throwable) {
                t.message
            }

            override fun onResponse(
                call: Call<Channel>,
                response: Response<Channel>
            ) {
                if (response.isSuccessful) {

                    var result = response.body()?.item
                    Log.i("log", Gson().toJson(result))
                }
            }
        })

    }
}
